package ee.parkingspot.customerapi.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the CustomerAccount entity.
 */
public class CustomerAccountDTO implements Serializable {

    private Long id;

    @NotNull
    @Size(min = 3)
    private String userName;

    @NotNull
    @DecimalMin(value = "0")
    private BigDecimal balance;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomerAccountDTO customerAccountDTO = (CustomerAccountDTO) o;
        if(customerAccountDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), customerAccountDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CustomerAccountDTO{" +
            "id=" + getId() +
            ", userName='" + getUserName() + "'" +
            ", balance='" + getBalance() + "'" +
            "}";
    }
}
