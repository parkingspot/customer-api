package ee.parkingspot.customerapi.service.mapper;

import ee.parkingspot.customerapi.domain.*;
import ee.parkingspot.customerapi.service.dto.CustomerAccountDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CustomerAccount and its DTO CustomerAccountDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CustomerAccountMapper extends EntityMapper<CustomerAccountDTO, CustomerAccount> {

    

    

    default CustomerAccount fromId(Long id) {
        if (id == null) {
            return null;
        }
        CustomerAccount customerAccount = new CustomerAccount();
        customerAccount.setId(id);
        return customerAccount;
    }
}
