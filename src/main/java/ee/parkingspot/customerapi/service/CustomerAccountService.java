package ee.parkingspot.customerapi.service;

import ee.parkingspot.customerapi.domain.CustomerAccount;
import ee.parkingspot.customerapi.repository.CustomerAccountRepository;
import ee.parkingspot.customerapi.service.dto.CustomerAccountDTO;
import ee.parkingspot.customerapi.service.mapper.CustomerAccountMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing CustomerAccount.
 */
@Service
@Transactional
public class CustomerAccountService {

    private final Logger log = LoggerFactory.getLogger(CustomerAccountService.class);

    private final CustomerAccountRepository customerAccountRepository;

    private final CustomerAccountMapper customerAccountMapper;

    public CustomerAccountService(CustomerAccountRepository customerAccountRepository, CustomerAccountMapper customerAccountMapper) {
        this.customerAccountRepository = customerAccountRepository;
        this.customerAccountMapper = customerAccountMapper;
    }

    /**
     * Save a customerAccount.
     *
     * @param customerAccountDTO the entity to save
     * @return the persisted entity
     */
    public CustomerAccountDTO save(CustomerAccountDTO customerAccountDTO) {
        log.debug("Request to save CustomerAccount : {}", customerAccountDTO);
        CustomerAccount customerAccount = customerAccountMapper.toEntity(customerAccountDTO);
        customerAccount = customerAccountRepository.save(customerAccount);
        return customerAccountMapper.toDto(customerAccount);
    }

    /**
     *  Get all the customerAccounts.
     *
     *  @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<CustomerAccountDTO> findAll() {
        log.debug("Request to get all CustomerAccounts");
        return customerAccountRepository.findAll().stream()
            .map(customerAccountMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     *  Get one customerAccount by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Transactional(readOnly = true)
    public CustomerAccountDTO findOne(Long id) {
        log.debug("Request to get CustomerAccount : {}", id);
        CustomerAccount customerAccount = customerAccountRepository.findOne(id);
        return customerAccountMapper.toDto(customerAccount);
    }

    /**
     *  Delete the  customerAccount by id.
     *
     *  @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete CustomerAccount : {}", id);
        customerAccountRepository.delete(id);
    }
}
