/**
 * View Models used by Spring MVC REST controllers.
 */
package ee.parkingspot.customerapi.web.rest.vm;
